
let store = {
    _nav:[
        {
            title: 'home',
            link: '/',
            img: 'home'
        },
        {
            title: 'About',
            link: '/about',
            img: 'user-tie'
        },
        {
            title: 'Skills',
            link: '/skills',
            img: 'tasks'
        },
        {
            title: 'Work',
            link: '/work',
            img: 'briefcase'
        },
        {
            title: 'Contact',
            link: '/contact',
            img: 'file-signature'
        }
    ],
    _social:[
        {
            title: 'Fb',
            link: 'https://www.facebook.com/',
            img: 'facebook-f'
        },
        {
            title: 'Inst',
            link: 'https://www.instagram.com/?hl=ru',
            img: 'instagram'
        },
        {
            title: 'VK',
            link: 'https://vk.com/',
            img: 'vk'
        },
    ],
    getNav(){
        return this._nav
    },
    getSocial(){
        return this._social
    },

    dispatch(action){

    }
};
export  default store