import React, { Component } from 'react';
import './App.css';
/*****
 * font fortawesome  for social and nav
 * instruction https://www.npmjs.com/package/@fortawesome/react-fontawesome
 * *****/
import { library } from '@fortawesome/fontawesome-svg-core'
import { fab } from '@fortawesome/free-brands-svg-icons'
import { fas} from '@fortawesome/free-solid-svg-icons'

/**
 * end
 */
import Nav from "./components/Navigation/Navigstion";
import {Switch, Route} from "react-router-dom";
import About from './components/About/About'
import Home from './components/Home/Home'
import Error404 from "./components/Error404/Error404";
import Skills from "./components/Skills/Skills";
import Work from "./components/Work/Work";
import Contact from "./components/Contact/Contact";



library.add(fab, fas)
const App = (props) => {
  return (
    <div className="wrapp_app">
          <Nav social={props.store.getSocial()} nav={props.store.getNav()}/>
          <div className="content">
              {/*<Route path={'/about'}*/}
              {/*render={() => <About/>}*/}
              {/*/>*/}
              <Switch>
                  <Route exact path={'/'} component={Home}/>
                  <Route exact path={'/about'} component={About}/>
                  <Route exact path={'/skills'} component={Skills}/>
                  <Route exact path={'/work'} component={Work}/>
                  <Route exact path={'/contact'} component={Contact}/>
                  <Route  path={'*'} component={Error404}/>
              </Switch>

          </div>
    </div>
  );
};

export default App;
