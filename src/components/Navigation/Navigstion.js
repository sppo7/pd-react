import React, { Component } from 'react';
import {NavLink} from "react-router-dom";
import s from "./Navigation.module.css"

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const Nav = (props) =>{

    let navigation = props.nav.map(
        (el) => {
            return(
                <li key={el.title}>
                    <NavLink exact activeClassName={s.active} to={el.link}>
                        <span>{el.title}</span>
                        <FontAwesomeIcon icon={[ 'fas' , el.img ]}  className={s.navIcon}/>
                    </NavLink>
                </li>
                )
        }
    );

    let social = props.social.map(
        (el) => {
            return(
                <li key={el.title}>
                <NavLink target={"_blank"} to={el.link}>
                    <FontAwesomeIcon icon={[ 'fab' , el.img ]}  className={s.socIcon}/>
                </NavLink>
                </li>
            )
        }
    );
    return(
        <div className={s.left_box}>
            <div className={s.logo}>
                <NavLink to={'/'}>
                    <img src={require("./img/logo.jpg")} alt=""/>
                </NavLink>
            </div>
            <div className={s.nav}>
                <ul>
                    {navigation}
                </ul>
            </div>
            <div className={s.social}>
                <ul>
                    {social}
                </ul>
            </div>
        </div>

    );
};
export default Nav;