import React, { Component } from 'react';
import s from './Contact.module.css'
import SimpleMap from "./Map";

const Contact = () =>{
    return(
        <div className="container h-100">
            <div className={'row h-100'}>

                <div className={s.form_box + ' width_40 flex-direction-c justify-content-center'}>
                    <h1 className={'h1 width_100'}>Contact me</h1>
                    <form action="" id={s.contactForm} className={'row'}>
                        <div className={s.control_form + ' width_50 pr-5 mb-10'}>
                            <input type="text" placeholder={'Name'}/>
                        </div>
                        <div className={s.control_form + ' width_50 pl-5 mb-10'}>
                            <input type="email" placeholder={'Email'}/>
                        </div>
                        <div className={s.control_form + ' width_100 mb-10'}>
                            <input type="tel" placeholder={'Phone'}/>
                        </div>
                        <div className={s.control_form + ' width_100 mb-10'}>
                            <textarea name="text"  rows="10" placeholder={'Message'}></textarea>
                        </div>
                        <div className={s.control_submit + ' width_100 justify-content-end d-flex'}>
                            <button type={'submit'} className={s.btn_send}>Send</button>
                        </div>

                    </form>
                </div>
                <div className={'width_60 pr-0'}>
                        <SimpleMap />
                </div>
            </div>
        </div>

    )
}
export  default Contact;