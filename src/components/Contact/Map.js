import React, { Component } from 'react';
import GoogleMapReact from 'google-map-react';

const AnyReactComponent = ({text}) => <div style={{width:'30px', height:'30px', border:'1px solid #000', display:'flex', alignItems: 'center', flexDirection: 'column'}}>
    <img style={{width:'100%', height: '100%'}} src={require("./logo.jpg")} alt={'Programing Developers'} />
    <div style={{textAlign:'center', marginTop:'5px', fontSize: '10px'}}>{text}</div>
</div>;
class SimpleMap extends Component {
    static defaultProps = {
        center: {
            lat: 50.615568,
            lng: 26.259517
        },
        zoom: 16
    };

    render() {
        return (
            <div style={{ height: '100%', width: '100%' }}>
                <GoogleMapReact
                    bootstrapURLKeys={{ key: 'AIzaSyBHeIwH3kftMVlIZyn2EvBjIqjBVcv4n1Q' }}
                    defaultCenter={this.props.center}
                    defaultZoom={this.props.zoom}
                >
                    <AnyReactComponent
                        lat = {50.615568}
                        lng = {26.259517}
                        text = "Programing Developers"
                    />
                </GoogleMapReact>
            </div>
        );
    }
}

export default SimpleMap;